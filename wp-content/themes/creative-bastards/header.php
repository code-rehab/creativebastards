<!doctype html>

<html <?php language_attributes(); ?>>
	<head>

	    <meta charset="<?php bloginfo( 'charset' ); ?>" />
	    <meta name="author" content="Creative Bastards" />
	    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
	    <link rel="shortcut icon" type="image/x-icon" href="<?php bloginfo('template_url'); ?>/assets/images/favicon.ico">

	    <title><?php wp_title(); ?></title>

	    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/assets/css/flipclock.css"  />
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/assets/css/site.css"  />
	    <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />

	    <script src="http://code.jquery.com/jquery-latest.min.js"></script>

	    <?php wp_head(); ?>
	</head>

	<?php
		$current_cat = get_query_var('category_name');
		if ($current_cat == 'whats-new') {
			header('Location: http://creativebastards.nl/'); die;
		}
	?>

	<body <?php body_class(get_device_info()); ?> >

		<aside id='main-menu'>
            <figure class='bee-structure'></figure>

			<div class="inner">
				<div class="fullheight">

					<a id="home-url" href="<?php bloginfo('url'); ?>" title="<?php bloginfo('name'); ?>">
						<figure class='logo' > </figure>
					</a>

					<nav>
						<?php language_selector_flags(); ?>
						<?php

						$walker = new My_Walker;
						wp_nav_menu(array(
							'theme_location' => 'aside-menu',
							'walker' => $walker
						));

						?>
					</nav>
				</div>
			</div>
		</aside>

		<header id='mobile-header'>
			<figure class='logo'> </figure>
			<div class='menu-button'></div>
		</header>
