<?php while ( have_posts() ) : the_post();?>

<?php
$thumbnail = wp_get_attachment_url( get_the_post_thumbnail( get_the_ID()));
$thumb_id = get_post_thumbnail_id();
$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'large', true);
$thumb_url = $thumb_url_array[0];
$content = get_post_field( 'post_content', get_the_ID() );
$content_parts = get_extended( $content );

if($_GET["popup"]) :
?>

<div class="content">
    <a href="" class="close-btn"></a>
    <img class="logo" src="<?php echo $thumb_url; ?>"/>
    <?php
    if (!empty(get_the_subtitle())) {
        echo '<h3>' . get_the_subtitle() . '</h3>';
    }
    ?>

    <?php
    if (!empty($content_parts['main'])) {
        echo '<p>' . $content_parts['main'] . '</p>';
    }
    ?>

    <a href="<?php echo get_the_permalink(); ?>" class="btn">LEES MEER</a>
</div>

<?php exit(); endif; ?>

<?php get_header();
$cta_title = get_post_meta( get_the_ID(), 'cta_title', true );
$cta_content = get_post_meta( get_the_ID(), 'cta_content', true );
$vimeo_id = get_post_meta( get_the_ID(), 'vimeo_id', true );
?>
<article class='pagewrap'>
    <?php if ( function_exists('yoast_breadcrumb') ) {
    yoast_breadcrumb('<p id="breadcrumbs">','</p>');
} ?>



    <?php if(!empty($vimeo_id)) {
    // convert url
    //$vimeo_url = str_replace('http://vimeo.com/','http://player.vimeo.com/video/',$vimeo_url);
    ?>

    <iframe class="vimeo" src="https://player.vimeo.com/video/<?= $vimeo_id?>" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
    <?php }
    else if (has_featured_image()){?>
    <img class='featured-image' src="<?= get_featured_image('url', 'full') ?>" width='100%'/>
    <?php } ?>


    <?php previous_post_link( '%link', '<span class="prev_post">' . __( '&nbsp;') . '</span>', true, '' ); ?>
    <?php next_post_link( '%link', '<span class="next_post">' . __( '&nbsp;') . '</span>', true, ''); ?>

    <?php previous_post_link('%link', '<span class="prev_post">' . __( '&nbsp') . '</span>', true, '', 'tax_portfolio' ); ?>
    <?php next_post_link( '%link', '<span class="next_post">' . __( '&nbsp') . '</span>', true, '', 'tax_portfolio' ); ?>

    <section id='pagecontainer' >

        <section class='main-content'>
            <h1 class='page-title color-1'><?php the_title(); ?></h1>
            <?php the_content(); ?>
            <span class='social-icons'>
                <ul>
                    <li class='icon pinterest'>

                        <?php

                        $url = get_permalink(get_the_ID());

                        echo '<a onclick="';
                        echo "window.open('http://pinterest.com/pin/create/button/?url=";
                        echo $url;
                        echo '&amp;media=' . get_featured_image('url', 'full');
                        echo '&amp;description=Merk: ' . $url;
                        echo "', 'pinit', 'height=320, width=640, toolbar=no, menubar=no, scrollbars=no, resizable=no, location=no, directories=no, status=no');";
                        echo '" href="javascript: void(0);">';
                        echo '</a>';
                        ?>

                    </li>

                    <li class='icon facebook'>
                        <?php


                        echo '<a style="margin-right:10px;" class="icon facebook" onclick="';
                        echo "window.open('https://www.facebook.com/sharer/sharer.php?u=" . $url . "', 'facebook_share', 'height=320, width=640, toolbar=no, menubar=no, scrollbars=no, resizable=no, location=no, directories=no, status=no');";
                        echo '" href="javascript: void(0);">';
                        echo '</a>';
                        ?>


                    </li>
                    <li class='icon twitter'>
                        <script>
                            $(document).ready(function(){
                                function tweet_btn(){
                                    // We bind a new event to our link
                                    $('a.tweet').click(function(e){

                                        //We tell our browser not to follow that link
                                        e.preventDefault();

                                        //We get the URL of the link
                                        var loc = $(this).attr('href');

                                        //We get the title of the link
                                        var title  = escape($(this).attr('title'));

                                        //We trigger a new window with the Twitter dialog, in the middle of the page
                                        window.open('http://twitter.com/share?url=' + loc + '&text=' + title + '&', 'twitterwindow', 'height=450, width=550, top='+($(window).height()/2 - 225) +', left='+$(window).width()/2 +', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
                                    });
                                }

                                tweet_btn();
                            });
                        </script>
                        <?php
                        echo '<a style="margin-right:10px;" href="' . $url . '" title="" class="tweet" target="_blank"></a>';

                        ?>

                    </li>
                </ul>
            </span>
        </section>

        <section class='call-to-action'>
            <h2 class='title'><?= $cta_title; ?></h2>
            <p><?= $cta_content; ?></p>
            <blockquote>
                <h3><a href="mailto:info@creativebastards.nl">Connect</a></h3>
            </blockquote>
        </section>

    </section>
</article>

<?php get_footer() ?>
<?php endwhile; ?>
