function swipeShowSidebar (event, phase, direction, distance){
    if (phase == "end" || phase == "cancel"){
        if ($(event.target).attr('href') && distance < 7) {
            if (phase == "end" || phase == "cancel"){
                $(event.target).unbind('click').click();
                return;
            }
        }else if (direction == 'left' && $('aside#main-menu').hasClass('opened')){
            is_closing = true;
            if ($('aside#main-menu .sub-menu .sub-menu').hasClass('expanded')){
                $('aside#main-menu .sub-menu .sub-menu').removeClass('expanded');
            }else{
                $('aside#main-menu .sub-menu').removeClass('expanded');
                $('aside#main-menu').removeClass('opened');
            }
        }
    }
};

var initialMenuHeight = $('aside#main-menu nav').outerHeight()-500;
$('aside#main-menu nav').css('margin-top', '-' + initialMenuHeight/2 + 'px');
$('aside#main-menu nav').css('opacity', '1');

var submenu = $('aside#main-menu nav .current-menu-item.menu-item-has-children .sub-menu');
if (!submenu.hasClass('expanded')){
    $('aside#main-menu .sub-menu').removeClass('expanded');
    submenu.addClass('expanded');
    submenu.css('height', (submenu.children('.menu-item:visible').length*28)+'px');
    newMenuHeight = ((submenu.children('.menu-item:visible').length*28)+$('aside#main-menu nav').outerHeight())-500;
    $('aside#main-menu nav').css('margin-top', '-' + newMenuHeight/2 + 'px');
}

$('header#mobile-header .menu-button').click(function(){
    if ($('aside#main-menu').hasClass('opened')){
        $('aside#main-menu').removeClass('opened');
        $('aside#main-menu .sub-menu').removeClass('expanded');
    }else $('aside#main-menu').addClass('opened');
});

$('aside#main-menu').swipe({allowPageScroll:'vertical', excludedElements:"", swipeStatus:swipeShowSidebar});

/*$('aside#main-menu .menu').children('.menu-item').find('a:first').click(function(e){
	var mItem = $(this).closest('.menu-item');
	var submenu = mItem.find('.sub-menu:first');

	if(mItem.hasClass('menu-item-has-children')) {
		if (!submenu.hasClass('expanded')){
			$('aside#main-menu .sub-menu').removeClass('expanded');
			submenu.addClass('expanded');
			submenu.css('height', (submenu.children('.menu-item:visible').length*28)+'px');
			newMenuHeight = ((submenu.children('.menu-item:visible').length*28)+$('aside#main-menu nav').outerHeight())-200;
			$('aside#main-menu nav').css('margin-top', '-' + newMenuHeight/2 + 'px');
		}else{
			$('aside#main-menu .sub-menu').removeClass('expanded');
			submenu.removeClass('expanded');
			submenu.css('height', '0px');
			$('aside#main-menu nav').css('margin-top', '-' + 0+ 'px');
		}
		//e.preventDefault();
	}
});*/


$('aside#main-menu .sub-menu').children('.menu-item').find('a:first').click(function(e){
    var mItem = $(this).closest('.menu-item');
    var submenu = mItem.find('.sub-menu:first');
    if(mItem.hasClass('menu-item-has-children')) {

        if (!submenu.hasClass('expanded')){
            $('aside#main-menu .sub-menu .sub-menu').removeClass('expanded');
            submenu.addClass('expanded');
        }else{
            submenu.removeClass('expanded');
        }

        e.preventDefault();
    }
});
