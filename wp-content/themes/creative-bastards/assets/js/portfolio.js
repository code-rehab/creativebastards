var $isotope_grid = false;
var $grid_items_per_row = 2;
var $isotope_filter = "";
var $lastpfs = $('.slideout-portfolio .item');
var viewerActive = false;
var $rowActive = 0;
var $currentPortfolioItem = false;
var $portfolioResizeTimer;
var viewerTimeToExpand = 500;
var $portfolioViewer;

$.ajaxSetup({cache:false});
$('#pagecontainer').css('opacity', '1');

$('.slideout-portfolio a.post-link').click(placeBigPortfolio);
$(".portfolio-item-viewer").on('click', ' .close-btn', closePortfolioViewer);

$(document).ready(function(){
  $isotope_grid = $('.slideout-portfolio').isotope({
    itemSelector: '.item',
    getSortData: {
      order: function( itemElem ) { // function
        return parseFloat($( itemElem ).attr('data-order'));
      },
    },
    sortBy: ['order'],
    layoutMode: 'fitRows',
    stagger: 30,
  });

  $portfolioViewer = $(".portfolio-item-viewer");

  portfolioSizeChanged();
});

function placeBigPortfolio(e){
  if(e) {
    e.preventDefault();
    viewerActive = true;
  }
  $lastpfs = $('.slideout-portfolio div.item' + (($isotope_filter) ? "." + $isotope_filter : ""));

  var target = (e) ? $(e.currentTarget) : false;
  var post_link = false;
  var itemsPerRow = $grid_items_per_row;
  var itemsLength = $lastpfs.length;
  var currentRow = 0

  if(!$portfolioViewer) $portfolioViewer = $(".portfolio-item-viewer");
  if(target){
    post_link   = target.attr("href")+"?popup=true";
    $currentPortfolioItem = target.closest('.item');
  }

  if($isotope_filter) $isotope_grid.isotope({filter: "." + $isotope_filter + ', .portfolio-item-viewer'});

  $isotope_grid.isotope('updateSortData').isotope();


  $.each($lastpfs, function(i, item){
    $(item).attr("data-order", i+1);

    currentRow = (i%itemsPerRow == 0) ? currentRow + 1 : currentRow;

    if($currentPortfolioItem && $(item).attr('id') == $currentPortfolioItem.attr('id')){
      var lastOfRow = (itemsPerRow * currentRow)
      var lastItem = $($lastpfs[lastOfRow-1])

      $portfolioViewer.removeClass('expanded');
      $portfolioViewer.attr('data-order', (lastOfRow) + ".1");

      if($rowActive != currentRow) {
        $rowActive = currentRow;
        $portfolioViewer.find('.inner').fadeOut('fast', function(){

          loadPortfolioViewerContent($portfolioViewer, post_link)
        });
      }else{
        if ( e )$portfolioViewer.find('.inner').fadeOut('fast', function(){
          loadPortfolioViewerContent($portfolioViewer, post_link)
        });
      }

      $isotope_grid.isotope('updateSortData').isotope();

    }
  })
}

function loadPortfolioViewerContent(viewer, post_link){
  if(viewer && post_link){
    viewer.find('.inner').load(post_link, function(){
      viewer.addClass('expanded');
      $(this).fadeIn();
      setTimeout(function(){
        $isotope_grid.isotope('updateSortData').isotope();
        moveToViewer();
      }, 200)
    });

  }
}

function closePortfolioViewer(e){
  e.preventDefault();
  $portfolioViewer.removeClass('expanded');
  $isotope_grid.isotope('updateSortData').isotope();
  viewerActive = false;
}

$('.menu-item.filter-menu .menu-item a').click(function(e){
  e.preventDefault();
  var url = $(e.currentTarget).attr('href');
  var filter = filter = url.replace("#filter-", "");

  if(filter == "none") $isotope_grid.isotope({filter: '*'});
  else {
    $isotope_grid.isotope({filter: "." + filter});
    $isotope_grid.isotope('updateSortData').isotope();
  }

  $isotope_filter = (filter == "none") ? "" : filter;
});

function portfolioSizeChanged() {
  var window_size = $(window).innerWidth();
  if(window_size < 450) $grid_items_per_row = 2;
  if(window_size >= 450) $grid_items_per_row = 3;
  if(window_size >= 768) $grid_items_per_row = 4;
  if(window_size >= 1600) $grid_items_per_row = 5;
  if(window_size >= 1920) $grid_items_per_row = 6;

  clearTimeout($portfolioResizeTimer);
  $portfolioResizeTimer = setTimeout(placeBigPortfolio, 500);

  $isotope_grid.isotope('updateSortData').isotope();
}

$(window).resize(portfolioSizeChanged);

function moveToViewer(){
  $windowheight = $(window).height();
  $viewerheight = $('.portfolio-item-viewer.item.expanded').outerHeight(true);
  $viewerposition = parseInt($('.portfolio-item-viewer.item.expanded').css("top"), 10);
  $itemheight = $('.item:not(.expanded)').first().outerHeight(true);
//
//  console.log($('.item:not(.expanded)').first().outerHeight(true));
////  console.log($windowheight);
  if( $viewerposition > ($windowheight-$itemheight)){
//    alert("ja")
    $("article.pagewrap").animate({ scrollTop: $viewerposition - (($windowheight-$viewerheight)/2)}, "normal");
  }
}
