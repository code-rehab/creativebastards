// Give @ a class
$(document).ready(function(){
  maxMargin();
  changeEmail();
  globalOnResize();
});

$(window).resize(globalOnResize);

function globalOnResize() {
  // vimeo aspect

  if($('.vimeo').length){
    var aspectRatio = 16/9;
    $('.vimeo').height($('.vimeo').width()/aspectRatio);
  }
}

function maxMargin(){
  $('span.subtitle').each(function(){
    var characters = $(this).text().length;
    var characterSpace = characters*15;

    $(this).css("max-width", characterSpace + "px");
  })
}

function changeEmail(){
  $('a.email:contains("@")').each(function(){
    var email = $(this).html().split('@');
    var newemail = email[0] + "<span class='at-sign'>@</span>" + email[1];

    $(this).html(newemail);
    $(this).wrap('<span class="email"></span>');
    $(this).html().replace('@', '<span class="email-at">@</span>');
  })
}
