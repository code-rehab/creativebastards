var newsslider;

if (window.innerHeight <= 768){
  $("#teammembers").css("opacity", "0");
  $("#teammembers").css("transition", "all 0.3s");
  $("#teammembers").css("-webkit-transition", "all 0.3s");
  $("#teammembers").css("-moz-transition", "all 0.3s");
}

Flickity.prototype.hasDragStarted = function( moveVector ) {
  // start dragging after pointer has moved 30 pixels in either direction
  return !this.isTouchScrolling && Math.abs( moveVector.x ) > 30;
};

$(document).ready(function(){

  initNewsSlider();

  animateMember();

  if (window.innerHeight <= 768){
    setTimeout(function(){
      newsslider.resize();
    }, 200)

    setTimeout(function(){

      $("#teammembers").css("opacity", "1");

    }, 700)

  }
});

var clientSliderIndex = 2;
var shortTimeout;
function initNewsSlider(){

  // NIEUWS Slider
  newsslider = new Flickity(".news-slider", {
    prevNextButtons: false,
    pageDots: false,
    wrapAround: true,
    initialIndex: clientSliderIndex,
  });

  newsslider.on( 'staticClick', function( event, pointer, cellElement, cellIndex ) {
    var slideID = cellIndex;
    var currentSlide = newsslider.selectedIndex;
    if(currentSlide != slideID){
      clientSliderIndex = slideID;
      newsslider.select(slideID);
    }
  })

  $(".news-slider .gallery-cell").click(function(e){

  })

  newsslider.on( 'settle', function() {

    if (window.innerHeight <= 768){
      clearTimeout(shortTimeout);
      shortTimeout = setTimeout(function(){
        newsslider.resize();
      }, 200)
    }
  });

  newsslider.on( 'cellSelect', function() {
    clientSliderIndex = newsslider.selectedIndex
    animateMember();
  });
}

$(document).keydown(function(e){
  if (e.keyCode == 37) {  //left
    newsslider.previous();
  }
});

$(document).keydown(function(e){
  if (e.keyCode == 39) {  //right
    newsslider.next();
  }
});

