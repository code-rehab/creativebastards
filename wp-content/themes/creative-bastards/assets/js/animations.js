var tl = new TimelineLite();

function animateMember(){
    var content = $('.is-selected section.content');

    tl.pause(tl.duration());
    tl.clear();
    tl = new TimelineLite();
    tl.timeScale(1.3);

    tl.from(content.find("span.subtitle"), 0.6, {
        //        delay: 0.6,
        maxWidth: 0,
        immediateRender: true
    }, "0.4");

    tl.from(content.find("p"), 1.6, {
        y: 200,
        opacity: 0,
        ease: Expo.easeOut,
        immediateRender: true
        //        delay: 0,
    }, "-=0.4");

    tl.staggerFrom(content.find("li.social-network"), 0.3, {
        y: 100,
        opacity: 0,
        immediateRender: true
    }, 0.1, "-=1.4");

    tl.from(content.find("div.social-network.telephone"), 0.3, {
        x: -400,
        opacity: 0,
        immediateRender: true
    }, "-=0.8");
}
