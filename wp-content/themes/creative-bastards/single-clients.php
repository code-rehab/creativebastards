<?php while ( have_posts() ) : the_post();?>

<?php
$thumbnail = wp_get_attachment_url( get_the_post_thumbnail( get_the_ID()));
$thumb_id = get_post_thumbnail_id();
$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'large', true);
$thumb_url = $thumb_url_array[0];
$content = get_post_field( 'post_content', get_the_ID() );
$content_parts = get_extended( $content );
$client = strval(get_the_ID());

if($_GET["popup"]) :

// Find portfolio items for this client
$args = array(
    'post_type'  => 'portfolio',
    'meta_query' => array(
        array(
            'key'   => 'portfolioclient',
            'value' => $post->ID
        )
    )
);

$posts = new WP_Query( $args );
$portfolioaantal = $posts->found_posts;
$subtitle = get_the_subtitle();
//var_dump($posts->posts[0]);
?>

<div class="content">
    <a href="" class="close-btn"></a>
    <img class="logo" src="<?php echo $thumb_url; ?>"/>
    <?php
    if (!empty($subtitle)) {
        echo '<h3>' . $subtitle . '</h3>';
    }
    ?>

    <?php
    if (!empty($content_parts['main'])) {
        echo '<p>' . $content_parts['main'] . '</p>';
    }
    ?>
    <?php if($portfolioaantal > 1 ){ ?>
    <a href="<?php echo get_the_permalink(); ?>" class="btn"><?php _e( 'View projects', 'creativebastards' ); ?></a>
    <?php } else if($portfolioaantal == 1 ){ ?>
    <a href="<?php echo get_permalink($posts->posts[0]->ID); ?>" class="btn"><?php _e( 'View project', 'creativebastards' ); ?>&nbsp;<?= $posts->posts[0]->post_title; ?></a>
    <?php } else if($portfolioaantal == 0) { ?>

    <?php } ?>
</div>

<?php exit(); endif; ?>

<?php get_header();

$args = array(
    'post_type' => 'portfolio',
);
$loop = new WP_Query( $args );
$i = 0;
?>

<article class='pagewrap'>
    <section id="portfolio" class="slideout-portfolio">
        <?php while ( $loop->have_posts() ) : $loop->the_post();

        $thumbnail = wp_get_attachment_url( get_the_post_thumbnail( get_the_ID()));
        $thumb_id = get_post_thumbnail_id();
        $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'large', true);
        $thumb_url = $thumb_url_array[0];
        $portfolioclient = get_post_meta(get_the_ID(), 'portfolioclient', true); //id
        $companylogo_image = get_post_meta(get_the_ID(), 'extra_featured_image', true);
        $i++;

        ?>

        <?php if ($client == $portfolioclient){   ?>

        <div id="pf-<?= $i ?>" data-order="<?= $i ?>" class="item <?= $filterclasses ?>" style="background: url('<?php echo $thumb_url ?>')">
            <a class="post-link" rel="<?php the_ID(); ?>" href="<?php the_permalink(); ?>">
                <div class="overlay">
                    <?php
                                               if (!empty($companylogo_image)) echo '<span><img src="' . $companylogo_image . '"/></span>';
                                               else echo '<span><h3>' . get_the_title() . '</h3></span>';
                    ?>
                </div>
            </a>
        </div>
        <?php } ?>

        <?php endwhile; ?>
        <section class="portfolio-item-viewer item"><div class="inner"></div></section>
    </section>

</article>

<?php get_footer() ?>
<?php endwhile; ?>
