<?php

add_action( 'init', 'create_clients_post_type' );
function create_clients_post_type() {

    $singular = 'Client';
    $plural = 'Clients';

    register_taxonomy('tax_clients',
                      'clients',
                      array("hierarchical" => true,
                            //                          "label" => "Diensten",
                            //                          "singular_label" => "Diensten",
                            'update_count_callback' => '_update_post_term_count',
                            'query_var' => true,
//                            'rewrite' => array( 'slug' => 'clients', 'with_front' => false ),
                            'menu_icon' => 'dashicons-admin-users',
                            'public' => true,'show_ui' => true,
                            'show_tagcloud' => true,
                            '_builtin' => false,
                            'show_in_nav_menus' => true
                           )
                     );

    register_post_type(
        'clients',
        array(
            'labels' => array(
                'name'               => __( $plural, 'coderehab-base' ),
                'singular_name'      => __( $singular, 'coderehab-base' ),
                'all_items'          => __( 'Alle ' . $plural, 'coderehab-base' ),
                'add_new'            => __( 'Nieuwe ' . $singular . ' toevoegen', 'coderehab-base' ),
                'add_new_item'       => __( 'Nieuwe ' . $singular . ' toevoegen', 'coderehab-base' ),
                'edit'               => __( 'Aanpassen', 'coderehab-base' ),
                'edit_item'          => __( $singular . ' toevoegen', 'coderehab-base' ),
                'new_item'           => __( 'Nieuwe ' . $singular, 'coderehab-base' ),
                'view'               => __( 'Bekijk ' . $singular, 'coderehab-base' ),
                'view_item'          => __( 'Bekijk ' . $singular, 'coderehab-base' ),
                'search_items'       => __( 'Zoek ' . $plural, 'coderehab-base' ),
                'not_found'          => __( 'Geen ' . $plural. ' gevonden', 'coderehab-base' ),
                'not_found_in_trash' => __( 'Geen ' . $plural. ' in de prullenbak gevonden', 'coderehab-base' ),
                'parent'             => __( 'Hoofd ' . $singular, 'coderehab-base' )
            ),
            'hierarchical' => true,
            'taxonomies' => array('tax_portfolio'),
            'show_in_nav_menus' => true,
            'public' => true,
            'has_archive' => true,

            'menu_position' => 5, // Onder berichten plaatsen
            'menu_icon'           => 'dashicons-admin-users',

//            'rewrite' => array(
//                'slug' => 'clients',
//                'with_front' => true
//            ),
            'supports' => array(
                'title',
                'editor',
                'thumbnail',
                //              'excerpt',
                //              'page-attributes'
            ),
        )
    );
}
/*
add_action( 'add_meta_boxes', 'what_we_do_meta_box_add' );
add_action( 'save_post', 'what_we_do_meta_box_save' );
function what_we_do_meta_box_add()
{
    add_meta_box( 'what_we_do-meta-data', 'what_we_do info', 'what_we_do_data_box', 'what_we_do', 'normal', 'high' );
}

function what_we_do_data_box( $post )
{
	$values = get_post_custom( $post->ID );
	$text = isset( $values['tagline'] ) ? esc_attr( $values['tagline'][0] ) : "";
?>
	<p>
	    <label for="tagline" style="display:block;">Tagline</label>
	    <textarea style="width:100%;" name="tagline" id="tagline" ><?php echo $text; ?></textarea>
    </p>
<?php
}

function what_we_do_meta_box_save( $post_id )
{
    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post' ) ) return;

	if( isset( $_POST['tagline'] ) )
        update_post_meta( $post_id, 'tagline', esc_attr( $_POST['tagline'] ) );

}*/
?>
