<?php

add_action( 'init', 'create_portfolio_post_type' );
function create_portfolio_post_type() {


	register_taxonomy('tax_portfolio',
		'portfolio',
		array("hierarchical" => true,
			"label" => "Diensten",
			"singular_label" => "Diensten",
			'update_count_callback' => '_update_post_term_count',
			'query_var' => true,
			'rewrite' => array( 'slug' => 'service', 'with_front' => false ),
			'public' => true,'show_ui' => true,
			'show_tagcloud' => true,
			'_builtin' => false,
			'show_in_nav_menus' => true
		)
	);

	register_post_type( 'portfolio',
		array(
			'labels' => array(
				'name' => __( 'Portfolio' ),
				'singular_name' => __( 'Portfolio' ),
				'parent_item_colon' => '',
				'add_new' => 'Nieuw item'
			),
			'hierarchical' => true,
			'taxonomies' => array('tax_portfolio'),
			'show_in_nav_menus' => true,
			'map_meta_cap' => true,
			'public' => true,
			'has_archive' => true,
			'rewrite' => array( 'slug' => 'portfolio','with_front' => FALSE),
			'supports' => array('title','editor','thumbnail','excerpt','comments', 'page-attributes'),
		)
	);
}
/*
add_action( 'add_meta_boxes', 'what_we_do_meta_box_add' );
add_action( 'save_post', 'what_we_do_meta_box_save' );
function what_we_do_meta_box_add()
{
    add_meta_box( 'what_we_do-meta-data', 'what_we_do info', 'what_we_do_data_box', 'what_we_do', 'normal', 'high' );
}

function what_we_do_data_box( $post )
{
	$values = get_post_custom( $post->ID );
	$text = isset( $values['tagline'] ) ? esc_attr( $values['tagline'][0] ) : "";
?>
	<p>
	    <label for="tagline" style="display:block;">Tagline</label>
	    <textarea style="width:100%;" name="tagline" id="tagline" ><?php echo $text; ?></textarea>
    </p>
<?php
}

function what_we_do_meta_box_save( $post_id )
{
    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post' ) ) return;

	if( isset( $_POST['tagline'] ) )
        update_post_meta( $post_id, 'tagline', esc_attr( $_POST['tagline'] ) );

}*/
?>
