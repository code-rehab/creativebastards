<?php

add_shortcode('teammembers', 'member_gallery');

function member_gallery($atts) {
  wp_enqueue_script( 'slider' );
  wp_enqueue_script( 'animations' );

  extract(shortcode_atts(array(

  ), $atts));

  //    global $device;

  $args = array(
    'post_type' => 'teammembers',
  );
  $loop = new WP_Query( $args );

  $output = "<div class='news-slider gallery'>";
  $i = 0;
  while ( $loop->have_posts() ) : $loop->the_post();

  $pinterest = get_post_meta( get_the_ID(), 'pinterest', true );
  $facebook = get_post_meta( get_the_ID(), 'facebook', true );
  $twitter = get_post_meta( get_the_ID(), 'twitter', true );
  $instagram = get_post_meta( get_the_ID(), 'instagram', true );
  $linkedin = get_post_meta( get_the_ID(), 'linkedin', true );
  $telephone = get_post_meta( get_the_ID(), 'telephone', true );
  $email = get_post_meta( get_the_ID(), 'email', true );
  $url = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) );
  $the_title = get_the_title();
  $subtitle = get_the_subtitle();
  
  $output .= '<div class="gallery-cell" data-cell-id="' .  $i . '">';
  $output .= "<section class='slide-img'>";
  $output .= "<img src='$url'/>";
  $output .= "<a href='".get_the_permalink()."' ></a>";
  $output .= "</section>";

  $output .= '<section class="content">';
  $output .= '<h3>'. $the_title;

  if ( ! empty( $the_title ) ) {
    $output .= '<span class="subtitle"><span>//</span>'.$subtitle.'</span></h3>';
  } else {
    $output .= '</h3>';
  };

  $output .= '<h4><a class="email" href="mailto:' . $email . '">' . $email . '</a></h4>';

  $output .= '<p>';
  $output .= get_the_content();
  $output .= '</p>';

  if ( ! empty( $pinterest ) || ! empty( $facebook ) || ! empty( $twitter ) || ! empty( $instagram ) || ! empty( $linkedin )){

    $output .= '<ul class="social-media">';

    if ( ! empty( $pinterest ) ) {
      $output .= '<li class="social-network pinterest"><a href="' . $pinterest . '" target="_blank" alt="Pinterest"></a></li>';
    }

    if ( ! empty( $facebook ) ) {
      $output .= '<li class="social-network facebook"><a href="' . $facebook . '" target="_blank" alt="Facebook"></a></li>';
    }

    if ( ! empty( $twitter ) ) {
      $output .= '<li class="social-network twitter"><a href="' . $twitter . '" target="_blank" alt="Twitter"></a></li>';
    }

    if ( ! empty( $instagram ) ) {
      $output .= '<li class="social-network instagram"><a href="' . $instagram . '" target="_blank" alt="Instagram"></a></li>';
    }

    if ( ! empty( $linkedin ) ) {
      $output .= '<li class="social-network linkedin"><a href="' . $linkedin . '" target="_blank" alt="LinkedIN"></a></li>';
    }

    $output .= '</ul>';

  }

  if ( ! empty( $telephone ) ) {
    $output .= '<div class="social-network telephone"><a href="tel:' . $telephone . '" target="_blank">' . $telephone . '</a></div>';
  }

  $output .= '</section>';
  $output .= '</div>';

  $i++;
  endwhile;

  wp_reset_query();

  $output .= '</div>';

  return $output;
}
