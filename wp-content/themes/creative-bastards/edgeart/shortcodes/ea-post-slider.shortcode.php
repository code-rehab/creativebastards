<?php
function add_ea_post_slider_func($atts) {
  extract(shortcode_atts(array(
    'id' => '',
    'class' => '',
    'title' => '',
    'category_name' => '',
    'post_type' => 'post',
    'width' => '-1',
    'height' => '-1',
    'count' => '-1',
    'orderby' => 'ID',
    'order' => 'DESC',
    'slide_count' => '5',
    'show_bullets' => true,
    'no_slide_labels' => false,
    'no_slide_description' => false
  ), $atts));

  global $device;

  $args = array(
    'posts_per_page' => $slide_count,
    'category_name' => "$category_name",
    'post_type' => "$post_type",
    'orderby' => $orderby,
    'order' => '$order',
  );

  $query = new WP_Query( $args );
  $output = "<section id='$id' class='$class sliderContainer allslider slide' >";

  while ( $query->have_posts() ) : $query->the_post();

  $has_description = get_post_meta( get_the_ID(), 'has_description', true );
  $has_labels = get_post_meta( get_the_ID(), 'has_labels', true );

  $header1 = get_post_meta( get_the_ID(), 'heading1', true );
  $header2 = get_post_meta( get_the_ID(), 'heading2', true );

  $output .= '<figure class="sliderElem" style="background: url(' . get_featured_image('url', 'full') .')">';
  //    $output .= "<section class='imageholder'>";

  //    if (!$device->isMobile())
  //        $output .= "<img class='slideimage' src='" . get_featured_image('url', 'full') . "' />";
  //    else if ($device->isTablet())
  //        $output .= "<img class='slideimage' src='" . get_featured_image('url', 'large') . "' />";
  //    else if ($device->isMobile() && !$device->isTablet())
  //        $output .= "<img class='slideimage' src='" . get_featured_image('url', 'medium') . "' />";
  //    $output .= "</section>";

  //if show_labels
  if ($has_labels && !$no_slide_labels):
  $output .= "<section class='labels'>";
  $output .= "<section class='inner'>";

  $output .= "<h1>" . $header1 . "</h1>";
  $output .= "<h2>" . $header2 . "</h2>";

  $output .= "</section>";
  $output .= "</section>";
  endif;
  //----

  $output .= "<a class='overlaybutton' href='" . get_permalink(get_the_ID()) . "'></a>";

  //if show_excerpt
  if ($has_description && !$no_slide_description):
  $output .= "<section class='description'>";
  $output .= "<section class='inner'>";
  $output .= "<h2 class='title'>" . get_the_title() . "</h2>";
  $output .= "<p class='content'>" . get_the_excerpt() . "</p>";

  $output .= "</section>";
  $output .= "</section>";
  endif;
  //----

  $output .= "</figure>";
  endwhile;

  if($title != '')$output .= "<h2 class='slider_title'>$title</h2>";
  if($show_bullets)$output .=  "<div class='bullets'></div>";

  $output .= "</section>";
  $output .= "</section>";
  $output .= "
  <script>
    var $id;
    jQuery(document).ready(function(){
      $id = new Flickity('#$id.sliderContainer', {
        prevNextButtons: false,
        pageDots: true,
        wrapAround: false,
        setGallerySize: false,
        cellSelector: '.sliderElem',
      });
    });
	</script>";

  return $output;
}

add_shortcode('ea-post-slider', 'add_ea_post_slider_func');
