<?php

add_action( 'add_meta_boxes', 'companylogo_select_meta_box_add' );
add_action( 'save_post', 'companylogo_select_meta_box_save' );

function companylogo_select_meta_box_add()
{
    add_meta_box( 'companylogo_select', 'Company logo', 'companylogo_select_menu_box', 'portfolio', 'normal', 'high' );
    add_meta_box( 'companylogo_select', 'Tegelweergave logo', 'companylogo_select_menu_box', 'clients', 'normal', 'high' );
}

function companylogo_select_menu_box( $post )
{
    $values = get_post_custom( $post->ID );
    $companylogo_button = isset( $values['extra_featured_image'] ) ? esc_attr( $values['extra_featured_image'][0] ) : "";

    include 'view.php';
}

function companylogo_select_meta_box_save( $post_id )
{

    // Defines
    $companylogo_button = $_POST["extra_featured_image"];

    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post' ) ) return;
    //var_dump( $_POST ); die;

    if( isset( $_POST['extra_featured_image'] ) ) {
        update_post_meta( $post_id, 'extra_featured_image', $companylogo_button);
    }
}
?>
