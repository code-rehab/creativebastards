<?php

add_action( 'add_meta_boxes', 'call_to_action_metabox_add' );
add_action( 'save_post', 'call_to_action_metabox_save' );
function call_to_action_metabox_add()
{
	add_meta_box( 'call_to_action_metabox', 'Call to Action content', 'call_to_action_metabox', 'portfolio', 'normal', 'high' );
}

function call_to_action_metabox( $post )
{
	$values = get_post_custom( $post->ID );
	$cta_title = isset( $values['cta_title'] ) ? esc_attr( $values['cta_title'][0] ) : "";
	$cta_content = isset( $values['cta_content'] ) ? esc_attr( $values['cta_content'][0] ) : "";
?>

<p>
	<label for="">Title</label><br />
	<input type="text" name='cta_title' id='cta_title' value="<?= $cta_title ?>" style="width:100%"/>
</p>

<p>
	<label for="">Description</label><br />
	<textarea name='cta_content' id='cta_content' style="width:100%; height:200px;"/><?= $cta_content ?></textarea>
</p>


<script>
	jQuery('#select_image').click(function(e){
		wp.media.editor.open(jQuery('#labels_props'));
		wp.media.editor.send.attachment = function(props, attachment){
			jQuery('.mobile-img-container').html('<img src="' + attachment.url + '" width="100%" />');
			jQuery('#mobile_image_url').val(attachment.url)
			console.log(props, attachment);
		}
	});
</script>
<?php
}

function call_to_action_metabox_save( $post_id )
{
	// Bail if we're doing an auto save
	if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
	// if our current user can't edit this post, bail
	if( !current_user_can( 'edit_post' ) ) return;


	if( isset( $_POST['cta_title'] ) )
		update_post_meta( $post_id, 'cta_title', $_POST['cta_title']);

	if( isset( $_POST['cta_content'] ) )
		update_post_meta( $post_id, 'cta_content', $_POST['cta_content']);
}

?>
