<div class="portfolioheader_selector">
    <p>
        <img id="portfolioheaderfeatured_image" style="max-width:100%;" src="<?php echo $portfolioheader_button ?>"/><br/>
        <input type="button" name="portfolioheader_image_button" id="portfolioheader_image_button" class="button" value="Kies een afbeelding" /><br/>
    </p>
    <p>
        <input hidden type="text" style="width:100%" id="portfolioheader_image" name="portfolioheader_image" value="<?php echo $portfolioheader_button ?>" />
    </p>

    <script>
        jQuery(document).ready(function($){
            var _custom_media = true,
                _orig_send_attachment = wp.media.editor.send.attachment;

            $('#portfolioheader_image_button').click(function(e) {
                var send_attachment_bkp = wp.media.editor.send.attachment;
                var button = $(this);
                var id = button.attr('id').replace('_button', '');
                _custom_media = true;
                wp.media.editor.send.attachment = function(props, attachment){
                    if ( _custom_media ) {
                        $("#portfolioheaderfeatured_image").attr('src', attachment.url);
                        $("#"+id).val(attachment.url);
                    } else {
                        return _orig_send_attachment.apply( this, [props, attachment] );
                    };
                }

                wp.media.editor.open(button);
                return false;
            });

            $('.add_media').on('click', function(){
                _custom_media = false;
            });
        });
    </script>
</div>
