<?php

add_action( 'add_meta_boxes', 'portfolioheader_select_meta_box_add' );
add_action( 'save_post', 'portfolioheader_select_meta_box_save' );

function portfolioheader_select_meta_box_add()
{
    add_meta_box( 'portfolioheader_select', 'Header Image', 'portfolioheader_select_menu_box', 'portfolio', 'normal', 'high' );
}

function portfolioheader_select_menu_box( $post )
{
    $values = get_post_custom( $post->ID );
    $portfolioheader_button = isset( $values['portfolioheader_image'] ) ? esc_attr( $values['portfolioheader_image'][0] ) : "";

    include 'view.php';
}

function portfolioheader_select_meta_box_save( $post_id )
{

    // Defines
    $portfolioheader_button = $_POST["portfolioheader_image"];

    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post' ) ) return;
    //var_dump( $_POST ); die;

    if( isset( $_POST['portfolioheader_image'] ) ) {
        update_post_meta( $post_id, 'portfolioheader_image', $portfolioheader_button);
    }
}
?>
