<?php

add_action( 'add_meta_boxes', 'portfolioclientselect_meta_box_add' );
add_action( 'save_post', 'portfolioclientselect_meta_box_save' );

function portfolioclientselect_meta_box_add()
{
    add_meta_box( 'portfolioclient', 'Portfolio client', 'portfolioclientselect_menu_box', 'portfolio', 'normal', 'high' );
}

function portfolioclientselect_menu_box( $post )
{
    $values = get_post_custom( $post->ID );
    $portfolioclient = isset( $values['portfolioclient'] ) ? esc_attr( $values['portfolioclient'][0] ) : "";

    include 'view.php';
}

function portfolioclientselect_meta_box_save( $post_id )
{

    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post' ) ) return;
    //var_dump( $_POST ); die;

    if( isset( $_POST['portfolioclient'] ) ) {
        update_post_meta( $post_id, 'portfolioclient', $_POST['portfolioclient']);
    }
}

?>
