
<div class="vimeo_selector">
    <p>Selecteer een video of plaats een vimeo video url in het veld "Vimeo Url"</p>
    <p><input class="button" name="vimeo_url_button" id="vimeo_url_button" value="Selecteer een video" /></p>
    <p><strong>Vimeo Url</strong></p>
    <input class='widefat' type="text" name="vimeo_url" id="vimeo_url" value="<?= $vimeo_url ?>"/>
    <?php  ?>
    <section class='selectedVideo' style="<?= ($vimeo_thumbnail) ? '' : 'display:none' ?>">
        <hr>
        <p><strong>Gekozen video</strong></p>
        <img id='vimeo_thumbnail' style='' width=100% src="<?= $vimeo_thumbnail ?>"/> 
        <p><strong>Korte omschrijving</strong></p>
        <textarea class='widefat' name="vimeo_description" rows="5" id="vimeo_description"><?= $vimeo_description ?></textarea>
    </section> 
</div> 

<script>

    jQuery(document).ready(function($)
                           {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;

        // ADJUST THIS to match the correct button
        $('.vimeo_selector .button').click(function(e) 
                                     {
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = $(this);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;
            wp.media.editor.send.attachment = function(props, attachment)
            {
                if ( _custom_media ) 
                {
                    $("#"+id).val(attachment.url);
                    onVideoChange();
                } else {
                    return _orig_send_attachment.apply( this, [props, attachment] );
                };
            }

            wp.media.editor.open(button);
            return false;
        });

        $('.add_media').on('click', function()
                           {
            _custom_media = false;
        });
        
        function getVimeoId( url ) {
            var match = /vimeo.*\/(\d+)/i.exec( url );
            if ( match ) {
                return match[1];
            }
        }
        
        var onVideoChange = function(e){

            var url = $('#vimeo_url').val();
            var regExp = /(http):\/\/(www\.)?vimeo.com\/(\d+)($|\/)/;
            var match = url.match(regExp);
 
                $.get("http://vimeo.com/api/v2/video/" + getVimeoId(url) + ".json", function(response){
                    console.log(response);
                    $('#vimeo_thumbnail').attr('src', response[0].thumbnail_large)
                    $('#vimeo_description').val(response[0].description)
                    $('.selectedVideo').css('display', 'block');
                });
            
            
        }
        
        $('#vimeo_url').change(onVideoChange);
    });

</script>