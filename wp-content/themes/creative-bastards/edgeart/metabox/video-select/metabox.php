<?php 

add_action( 'add_meta_boxes', 'video_select_meta_box_add' );
add_action( 'save_post', 'video_select_meta_box_save' );

function video_select_meta_box_add()
{
    add_meta_box( 'video_select', 'Vimeo', 'video_select_menu_box', 'post', 'normal', 'high' );
    add_meta_box( 'video_select', 'Vimeo', 'video_select_menu_box', 'portfolio', 'normal', 'high' );
}

function video_select_menu_box( $post )
{
    $values = get_post_custom( $post->ID );
    $vimeo_thumbnail = isset( $values['vimeo_thumbnail'] ) ? esc_attr( $values['vimeo_thumbnail'][0] ) : "";
    $vimeo_id = isset( $values['vimeo_id'] ) ? esc_attr( $values['vimeo_id'][0] ) : "";
    $vimeo_url = isset( $values['vimeo_url'] ) ? esc_attr( $values['vimeo_url'][0] ) : "";
    $vimeo_description = isset( $values['vimeo_description'] ) ? esc_attr( $values['vimeo_description'][0] ) : "";
    
    
    include 'view.php';
}

function video_select_meta_box_save( $post_id )
{

    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post' ) ) return;
    //var_dump( $_POST ); die;
    
    if( isset( $_POST['vimeo_url'] ) ) {
        
        $vimeo = $_POST['vimeo_url'];
        $vimeo_id = (int) substr(parse_url($vimeo, PHP_URL_PATH), 1);
        $vid = file_get_contents("http://vimeo.com/api/v2/video/$vimeo_id.json");
        
        update_post_meta( $post_id, 'vimeo_url', $vimeo);
        update_post_meta( $post_id, 'vimeo_id', $vimeo_id);
        //update_post_meta( $post_id, 'vimeo_thumbnail', json_decode($vid)[0]->thumbnail_large);
        //update_post_meta( $post_id, 'vimeo_description', json_decode($vid)[0]->description);
    
    }
    
}

?>
