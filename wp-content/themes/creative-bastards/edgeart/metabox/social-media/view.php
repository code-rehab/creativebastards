
<div class="social-media">
    <p>De social media accounts voor deze teammember</p>

    <p><strong>Pinterest</strong></p>
    <input class='widefat' type="text" name="pinterest" id="pinterest" value="<?= $pinterest ?>"/>

    <p><strong>Facebook</strong></p>
    <input class='widefat' type="text" name="facebook" id="facebook" value="<?= $facebook ?>"/>

    <p><strong>Twitter</strong></p>
    <input class='widefat' type="text" name="twitter" id="twitter" value="<?= $twitter ?>"/>

    <p><strong>Instagram</strong></p>
    <input class='widefat' type="text" name="instagram" id="instagram" value="<?= $instagram ?>"/>

    <p><strong>LinkedIN</strong></p>
    <input class='widefat' type="text" name="linkedin" id="linkedin" value="<?= $linkedin ?>"/>

    <p><strong>Telefoonnummer</strong></p>
    <input class='widefat' type="text" name="telephone" id="telephone" value="<?= $telephone ?>"/>

    <p><strong>E-mailadres</strong></p>
    <input class='widefat' type="text" name="email" id="email" value="<?= $email ?>"/>
</div>
