<?php

add_action( 'add_meta_boxes', 'socialmedia_meta_box_add' );
add_action( 'save_post', 'socialmedia_meta_box_save' );

function socialmedia_meta_box_add()
{
    //add_meta_box( 'socialmedia', 'Vimeo', 'socialmedia_menu_box', 'post', 'normal', 'high' );
    add_meta_box( 'socialmedia', 'Social Media', 'socialmedia_menu_box', 'teammembers', 'normal', 'high' );
}

function socialmedia_menu_box( $post )
{
    $values = get_post_custom( $post->ID );
    $pinterest = isset( $values['pinterest'] ) ? esc_attr( $values['pinterest'][0] ) : "";
    $facebook = isset( $values['facebook'] ) ? esc_attr( $values['facebook'][0] ) : "";
    $twitter = isset( $values['twitter'] ) ? esc_attr( $values['twitter'][0] ) : "";
    $instagram = isset( $values['instagram'] ) ? esc_attr( $values['instagram'][0] ) : "";
    $linkedin = isset( $values['linkedin'] ) ? esc_attr( $values['linkedin'][0] ) : "";
    $telephone = isset( $values['telephone'] ) ? esc_attr( $values['telephone'][0] ) : "";
    $email = isset( $values['email'] ) ? esc_attr( $values['email'][0] ) : "";

    include 'view.php';
}

function socialmedia_meta_box_save( $post_id )
{

    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post' ) ) return;
    //var_dump( $_POST ); die;

    if( isset( $_POST['pinterest'] ) ) {
        update_post_meta( $post_id, 'pinterest', $_POST['pinterest']);
    }

    if( isset( $_POST['facebook'] ) ) {
        update_post_meta( $post_id, 'facebook', $_POST['facebook']);
    }

    if( isset( $_POST['twitter'] ) ) {
        update_post_meta( $post_id, 'twitter', $_POST['twitter']);
    }

    if( isset( $_POST['instagram'] ) ) {
        update_post_meta( $post_id, 'instagram', $_POST['instagram']);
    }

    if( isset( $_POST['linkedin'] ) ) {
        update_post_meta( $post_id, 'linkedin', $_POST['linkedin']);
    }

    if( isset( $_POST['telephone'] ) ) {
        update_post_meta( $post_id, 'telephone', $_POST['telephone']);
    }

        if( isset( $_POST['email'] ) ) {
        update_post_meta( $post_id, 'email', $_POST['email']);
    }
}

?>
