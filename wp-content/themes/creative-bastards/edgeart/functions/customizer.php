<?php

function coderehab_customize_register($wp_customize){

    // -- Social Media --------------------------------------------------------------------------------------------------

    $wp_customize->add_section( 'coderehab_social', array(
        'priority'		    => 1,
        'title'     	    => __( 'Social Media', 'coderehab' ),
    ) );

    // Facebook
    $wp_customize->add_setting( 'coderehab_social_facebook' , array(
        'default'     		=> __( 'https://www.facebook.com/code.rehab/', 'coderehab' ),
        'type' 				=> 'option',
        'capability'        => 'edit_theme_options',
        'transport' 		=> 'refresh',
        'sanitize_callback'	=> 'esc_html'
    ) );

    $wp_customize->add_control( 'coderehab_social_facebook', array(
        'type'              => 'text',
        'label'             => __( 'Facebook URL', 'coderehab' ),
        'section'           => 'coderehab_social',
        'settings'          => 'coderehab_social_facebook',
        'priority'          => 1
    ) );

    // Twitter
    $wp_customize->add_setting( 'coderehab_social_twitter' , array(
        'default'     		=> __( 'https://twitter.com/coderehab', 'coderehab' ),
        'type' 				=> 'option',
        'capability'        => 'edit_theme_options',
        'transport' 		=> 'refresh',
        'sanitize_callback'	=> 'esc_html'
    ) );

    $wp_customize->add_control( 'coderehab_social_twitter', array(
        'type'              => 'text',
        'label'             => __( 'Twitter', 'coderehab' ),
        'section'           => 'coderehab_social',
        'settings'          => 'coderehab_social_twitter',
        'priority'          => 2
    ) );

    // LinkedIN
    $wp_customize->add_setting( 'coderehab_social_linkedin' , array(
        'default'     		=> __( 'https://www.linkedin.com/company/code-rehab', 'coderehab' ),
        'type' 				=> 'option',
        'capability'        => 'edit_theme_options',
        'transport' 		=> 'refresh',
        'sanitize_callback'	=> 'esc_html'
    ) );

    $wp_customize->add_control( 'coderehab_social_linkedin', array(
        'type'              => 'text',
        'label'             => __( 'LinkedIN', 'coderehab' ),
        'section'           => 'coderehab_social',
        'settings'          => 'coderehab_social_linkedin',
        'priority'          => 3
    ) );

    // Instagram
    $wp_customize->add_setting( 'coderehab_social_instagram' , array(
        'default'     		=> __( 'https://www.instagram.com', 'coderehab' ),
        'type' 				=> 'option',
        'capability'        => 'edit_theme_options',
        'transport' 		=> 'refresh',
        'sanitize_callback'	=> 'esc_html'
    ) );

    $wp_customize->add_control( 'coderehab_social_instagram', array(
        'type'              => 'text',
        'label'             => __( 'Instagram', 'coderehab' ),
        'section'           => 'coderehab_social',
        'settings'          => 'coderehab_social_instagram',
        'priority'          => 3
    ) );

    // Pinterest
    $wp_customize->add_setting( 'coderehab_social_pinterest' , array(
        'default'     		=> __( 'https://www.pinterest.com', 'coderehab' ),
        'type' 				=> 'option',
        'capability'        => 'edit_theme_options',
        'transport' 		=> 'refresh',
        'sanitize_callback'	=> 'esc_html'
    ) );

    $wp_customize->add_control( 'coderehab_social_pinterest', array(
        'type'              => 'text',
        'label'             => __( 'Pinterest', 'coderehab' ),
        'section'           => 'coderehab_social',
        'settings'          => 'coderehab_social_pinterest',
        'priority'          => 3
    ) );

    // Telephone
    $wp_customize->add_setting( 'coderehab_social_telephone' , array(
        'default'     		=> __( '06123456789', 'coderehab' ),
        'type' 				=> 'option',
        'capability'        => 'edit_theme_options',
        'transport' 		=> 'refresh',
        'sanitize_callback'	=> 'esc_html'
    ) );

    $wp_customize->add_control( 'coderehab_social_telephone', array(
        'type'              => 'text',
        'label'             => __( 'Telefoonnummer', 'coderehab' ),
        'section'           => 'coderehab_social',
        'settings'          => 'coderehab_social_telephone',
        'priority'          => 3
    ) );

}
add_action( 'customize_register', 'coderehab_customize_register' );

// -- Waardes ophalen --------------------------------------------------------------------------------------------------
//
// get_theme_mod( 'coderehab_location', '' )
// get_options('coderehab_location', '' )

// -- Soorten Controls --------------------------------------------------------------------------------------------------

// ---- Default ---------------------------------------------------------------------------------------------------------

//$wp_customize->add_control( 'setting_id', array(
//  'type' => 'date', // of checkbox, textarea, radio, select of dropdown-pages
//  'priority' => 10, // Within the section.
//  'section' => 'colors', // Required, core or custom.
//  'label' => __( 'Date' ),
//  'description' => __( 'This is a date control with a red border.' ),
//  'input_attrs' => array(
//    'class' => 'my-custom-class-for-js',
//    'style' => 'border: 1px solid #900',
//    'placeholder' => __( 'mm/dd/yyyy' ),
//  ),
//  'active_callback' => 'is_front_page',
//) );

// ---- Custom CSS ---------------------------------------------------------------------------------------------------------

//$wp_customize->add_control( 'custom_theme_css', array(
//  'label' => __( 'Custom Theme CSS' ),
//  'type' => 'textarea',
//  'section' => 'custom_css',
//) );

// ---- Range Slider ---------------------------------------------------------------------------------------------------------

//$wp_customize->add_control( 'setting_id', array(
//  'type' => 'range',
//  'section' => 'title_tagline',
//  'label' => __( 'Range' ),
//  'description' => __( 'This is the range control description.' ),
//  'input_attrs' => array(
//    'min' => 0,
//    'max' => 10,
//    'step' => 2,
//  ),
//) );

// ---- Color Control ---------------------------------------------------------------------------------------------------------

//$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'color_control', array(
//  'label' => __( 'Accent Color', 'theme_textdomain' ),
//  'section' => 'media',
//) ) );
