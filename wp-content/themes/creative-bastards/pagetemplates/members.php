<?php
/*
Template Name: Teammembers Overview
*/
get_header();
?>

<article class='pagewrap' <?= get_featured_image('style', 'full') ?>>
    <section id="teammembers" style="overflow:hidden">
        <?php echo do_shortcode('[teammembers]');?>
    </section>
    <footer>
        <span class="footer-info"><a class="phone" href="tel:<?php echo get_option('coderehab_social_telephone', ''); ?>"><?php echo get_option('coderehab_social_telephone', ''); ?></a><span> // </span><a href="mailto:info@creativebastards.com" class="email">info@creativebastards.com</a></span>
        <ul class="social-media">
            <?php
            $facebook = get_option('coderehab_social_pinterest', '');
            $pinterest = get_option('coderehab_social_pinterest', '');
            $twitter = get_option('coderehab_social_pinterest', '');

            if ( ! empty( $pinterest ) ) {
                echo '<li class="social-network pinterest"><a href="' . get_option('coderehab_social_pinterest', '') . '" target="_blank" alt="Pinterest"></a></li>';
            }

            if ( ! empty( $facebook ) ) {
                echo '<li class="social-network facebook"><a href="' . get_option('coderehab_social_facebook', '') . '" target="_blank" alt="Facebook"></a></li>';
            }

            if ( ! empty( $twitter ) ) {
                echo '<li class="social-network twitter"><a href="' . get_option('coderehab_social_twitter', '') . '" target="_blank" alt="Twitter"></a></li>';
            }
            ?>
        </ul>
    </footer>
</article>
<?php get_footer() ?>





