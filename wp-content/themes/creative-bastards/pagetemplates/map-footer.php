<?php
/*
Template Name: map-footer
*/
get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>
<article class='pagewrap' <?= get_featured_image('style', 'full') ?>>

  <section id='pagecontainer' >
    <h1 class='page-title color-1'><?php the_title(); ?></h1>
    <?php the_content(); ?>
  </section>

  <!--        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2444.261753114988!2d6.874075799999994!3d52.22046209999993!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47b8138dfa4a06c7%3A0x4a547ca1850075a8!2sParkweg+55%2C+7545+MT+Enschede!5e0!3m2!1snl!2snl!4v1413386374876" width="100%" height="50%" frameborder="0" style="border:0"></iframe>-->

  <!--Iframe new style-->
  <style type="text/css">
    #map {
      width: 100%;
      height: 50%;
    }
  </style>

  <!--
## https://snazzymaps.com/editor/ voor het maken van maps ##


You need to include this script tag on any page that has a Google Map.

The following script tag will work when opening this example locally on your computer.
But if you use this on a localhost server or a live website you will need to include an API key.
Sign up for one here (it's free for small usage):
https://developers.google.com/maps/documentation/javascript/tutorial#api_key

After you sign up, use the following script tag with YOUR_GOOGLE_API_KEY replaced with your actual key.
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_GOOGLE_API_KEY&sensor=false"></script>
-->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCqdWu7sE8oNd5tmUSdyWGfxDjwiPWYNiA&amp;sensor=false"></script>
<!--  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>-->

  <script type="text/javascript">
    // When the window has finished loading create our google map below
    google.maps.event.addDomListener(window, 'load', init);

    function init() {
      // Basic options for a simple Google Map
      // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
      var mapOptions = {

        // Own adds
        mapTypeControl: false,
        scrollwheel: false,
        disableDefaultUI: true,

        // How zoomed in you want the map to start at (always required)
        zoom: 16,

        // The latitude and longitude to center the map (always required)
        center: new google.maps.LatLng(52.220462, 6.874076), // Parkweg 55, 7545 MT Enschede, the Netherlands

        // How you would like to style the map.
        styles:
        [
          {
            "stylers": [
              { "hue": "#ffee00" },
              { "invert_lightness": true },
              { "saturation": -100 },
              { "gamma": 0.9 },
              { "lightness": 13 }
            ]
          },{
            "featureType": "road.arterial",
            "stylers": [
              { "saturation": 100 },
              { "visibility": "simplified" },
              { "lightness": 43 },
              { "hue": "#ffd500" },
              { "gamma": 0.86 }
            ]
          },{
            "featureType": "road",
            "elementType": "labels.text.fill",
            "stylers": [
              { "hue": "#ff0000" },
              { "visibility": "on" },
              { "saturation": -100 },
              { "weight": 3.6 },
              { "invert_lightness": true },
              { "gamma": 2.33 },
              { "lightness": -5 }
            ]
          },{
            "elementType": "labels.text.stroke",
            "stylers": [
              { "visibility": "on" },
              { "gamma": 0.01 },
              { "saturation": -100 },
              { "lightness": -100 },
              { "weight": 0.9 }
            ]
          },{
            "featureType": "poi.business"  }
        ]
      };

      // Get the HTML DOM element that will contain your map
      var mapElement = document.getElementById('map');

      // Create the Google Map using our element and options defined above
      var map = new google.maps.Map(mapElement, mapOptions);

      // Let's also add a marker while we're at it
      var marker = new google.maps.Marker({
        position: new google.maps.LatLng(52.220462, 6.874076), // Parkweg 55, 7545 MT Enschede, the Netherlands
        map: map,
        title: 'creative bastards'
      });
    }
  </script>
  <div id="map"></div>
</article>
<?php endwhile; ?>
<?php get_footer() ?>
