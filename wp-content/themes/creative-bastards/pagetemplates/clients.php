<?php
/*
Template Name: Clients
*/
get_header();
?>

<?php
$args = array(
  'post_type' => 'clients',
);
$loop = new WP_Query( $args );
$i = 0;
?>

<article class='pagewrap'>
  <section id="clients" class="slideout-portfolio">
    <?php while ( $loop->have_posts() ) : $loop->the_post();

    $thumbnail = wp_get_attachment_url( get_the_post_thumbnail( get_the_ID()));
    $thumb_id = get_post_thumbnail_id();
    $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'large', true);
    $thumb_url = $thumb_url_array[0];
//    $filterclasses = "";
//    foreach (get_the_terms(get_the_ID(), "tax_portfolio") as $category){
//      $filterclasses .= " " . $category->slug;
//    }
    $companylogo_image = get_post_meta(get_the_ID(), 'extra_featured_image', true);

    $i++;
    ?>

    <?php
    if (!empty($companylogo_image)) {
      echo '<div id="pf-' . $i . '" data-order="' . $i . '" class="item ' . $filterclasses . '">';
    ?>

    <a class="post-link" rel="<?php the_ID(); ?>" href="<?php the_permalink(); ?>">
      <div class="overlay"  style="background-image: url('<?= $companylogo_image ?>')"></div>
    </a>
    </div>

  <?php } endwhile; ?>

  <section class="portfolio-item-viewer item"><div class="inner"></div></section>
  </section>

</article>

<?php get_footer() ?>





