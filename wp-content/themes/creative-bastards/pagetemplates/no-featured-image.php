<?php
/*
Template Name: no-featured-image
*/
get_header();
?>

	<?php while ( have_posts() ) : the_post();
		$cta_title = get_post_meta( get_the_ID(), 'cta_title', true );
		$cta_content = get_post_meta( get_the_ID(), 'cta_content', true );
	?>
	<article class='pagewrap'>
		<?php if ( function_exists('yoast_breadcrumb') ) {
		yoast_breadcrumb('<p id="breadcrumbs">','</p>');
		} ?>
		<?php if (has_featured_image()): ?>
			<img class='featured-image' src="<?= get_featured_image('url', 'full') ?>" width='100%'/>
		<?php endif; ?>

		<?php previous_post_link( '%link', '<span class="prev_post">' . __( '&nbsp;') . '</span>', true, '' ); ?>
		<?php next_post_link( '%link', '<span class="next_post">' . __( '&nbsp;') . '</span>', true, ''); ?>

		<?php previous_post_link('%link', '<span class="prev_post">' . __( '&nbsp') . '</span>', true, '', 'tax_portfolio' ); ?>
		<?php next_post_link( '%link', '<span class="next_post">' . __( '&nbsp') . '</span>', true, '', 'tax_portfolio' ); ?>

		<section id='pagecontainer' >

			<section class='main-content'>
				<h1 class='page-title color-1'><?php the_title(); ?></h1>
				<?php the_content(); ?>
				<span class='social-icons'>
					<ul>
						<li class='icon pinterest'><a href='' ></a></li>
						<li class='icon facebook'><a href='' ></a></li>
						<li class='icon twitter'><a href='' ></a></li>
					</ul>
				</span>
			</section>

			<section class='call-to-action'>
				<h2 class='title'><?= $cta_title; ?></h2>
				<p><?= $cta_content; ?></p>
				<blockquote>
					<h3><a href="mailto:info@creativebastards.nl">Connect</a></h3>
				</blockquote>
			</section>

		</section>
	</article>
	<?php endwhile; ?>
<?php get_footer() ?>
