<?php
/*
Template Name: What's new
*/
get_header();
?>

<article class='pagewrap'>
    <section id='pagecontainer'  style="overflow:hidden">

		<?php
			echo do_shortcode('[ea-post-slider id="slider_news" class="watsnew_slider" category_name="whats-new" title="' . __( 'What\'s new', 'creativebastards' ) . '"]');
			echo do_shortcode('[ea-post-slider id="slider_about" class="watsnew_slider" category_name="about-us" title="' . __( 'About us', 'creativebastards' ) . '" no_slide_description=true]');
			echo do_shortcode('[ea-post-slider id="slider_services" class="watsnew_slider" post_type="portfolio" title="' . __( 'What we do', 'creativebastards' ) . '" orderby="rand" no_slide_description=true]');
		?>

		<script>
			$(function(){
				function onWindowResize(){
					slider_news.onResizeHandler();
					slider_about.onResizeHandler();
					slider_services.onResizeHandler();
					onWindowResizeFunc();
				}
				window.onresize = onWindowResize;
			})
		</script>
        
	</section>
</article>

<?php get_footer() ?>Who doesn’t. Just hit the 'connect' button to get in touch. Let us know what your problem is and we’ll see if we can fix it. We usually do.Who doesn’t. Just hit the 'connect' button to get in touch. Let us know what your problem is and we’ll see if we can fix it. We usually do.