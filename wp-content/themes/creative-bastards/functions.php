<?php

// Register JS files
function register_script_styles() {

    // Required
    wp_enqueue_script( 'touchswipe', get_template_directory_uri() . '/assets/js/external/jquery.touchSwipe.min.js', array(), '1.0.0', true );

    // Optional
    wp_register_script( 'flipclock', get_template_directory_uri() . '/assets/js/external/flipclock.min.js', array(), '1.0.0', true );
    wp_register_script( 'midway', get_template_directory_uri() . '/assets/js/external/midway.min.js', array(), '1.0.0', true );

    wp_enqueue_script( 'isotope', get_template_directory_uri() . '/assets/js/external/jquery.isotope.min.js', array(), '1.0.0', true );
    wp_enqueue_script( 'flickity', get_template_directory_uri() . '/assets/js/external/jquery.flickity.min.js', array(), '1.0.0', true );

    wp_enqueue_script( 'slider-old', get_template_directory_uri() . '/assets/js/slider.min.js', array(), '1.0.0', true );

    wp_enqueue_script( 'tweenlite', get_template_directory_uri() . '/assets/js/external/TweenLite.min.js', array(), '1.0.0', true );
    wp_enqueue_script( 'timelinelite', get_template_directory_uri() . '/assets/js/external/TimelineLite.min.js', array(), '1.0.0', true );
    wp_enqueue_script( 'CSSplugin', get_template_directory_uri() . '/assets/js/external/plugins/CSSPlugin.min.js', array(), '1.0.0', true );
    wp_enqueue_script( 'EasePack', get_template_directory_uri() . '/assets/js/external/plugins/EasePack.min.js', array(), '1.0.0', true );


    wp_enqueue_script( 'sidemenu', get_template_directory_uri() . '/assets/js/sidemenu.class.js', array(), '1.0.0', true );
    wp_enqueue_script( 'site', get_template_directory_uri() . '/assets/js/site.js', array(), '1.0.0', true );
    wp_enqueue_script( 'menu', get_template_directory_uri() . '/assets/js/menu.js', array(), '1.0.0', true );

    // Optional
    wp_enqueue_script( 'slider', get_template_directory_uri() . '/assets/js/slider.js', array(), '1.0.0', true );
    wp_enqueue_script( 'animations', get_template_directory_uri() . '/assets/js/animations.js', array(), '1.0.0', true );
    wp_enqueue_script( 'portfolio', get_template_directory_uri() . '/assets/js/portfolio.js', array(), '1.0.0', true );
}
add_action( 'wp_enqueue_scripts', 'register_script_styles' );

//error_reporting(E_ALL);
ini_set('display_errors', 1);

require 'edgeart/functions/mobile-detect.php';
require 'edgeart/functions/featured-image.php';
require 'edgeart/functions/customizer.php';

require 'edgeart/c_post_types/portfolio.ptype.php';
require 'edgeart/c_post_types/teammembers.ptype.php';
require 'edgeart/c_post_types/clients.ptype.php';

require 'edgeart/shortcodes/countdown-timer.php';
require 'edgeart/shortcodes/ea-post-slider.shortcode.php';
require 'edgeart/shortcodes/teammembers.scode.php';

require 'edgeart/metabox/slide_properties.php';
require 'edgeart/metabox/mobile-image.php';
require 'edgeart/metabox/call-to-action-text.php';
require 'edgeart/metabox/company-logo/metabox.php';
require 'edgeart/metabox/social-media/metabox.php';
require 'edgeart/metabox/portfolio-header/metabox.php';
require 'edgeart/metabox/client-select/metabox.php';
//require 'edgeart/metabox/featured-vimeo.php';

require 'edgeart/metabox/video-select/metabox.php';

add_action( 'after_setup_theme', 'register_theme_menus' );
function register_theme_menus() {
    register_nav_menus( array(
        'aside-menu' => __( 'Hoofdmenu' ),
        'sidebar-menu' => __( 'Menu sidebar' ),
    ));
}

add_action( 'init', 'my_theme_add_editor_styles' );
function my_theme_add_editor_styles() {
    add_editor_style( 'assets/css/wp-editor-style.css' );
}

add_theme_support( 'post-thumbnails', array('post', 'page', 'portfolio', 'teammembers', 'clients') );

function my_mce4_options( $init ) {
    $default_colours = '[
    "000000", "Black",
    "ffffff", "White",
    "fc0", "Yellow",
    "242424", "gray",
]';

    $init['textcolor_map'] = $default_colours; // build colour grid default+custom colors
    $init['textcolor_rows'] = 6; // enable 6th row for custom colours in grid
    return $init;
}
add_filter('tiny_mce_before_init', 'my_mce4_options');

class My_Walker extends Walker_Nav_Menu
{

  function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
        global $wp_query;
        $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
 
        $class_names = $value = '';

        $classes = empty( $item->classes ) ? array() : (array) $item->classes;

        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
        $class_names = ' class="' . esc_attr( $class_names ) . '"';

        $output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';

        $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
        $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
        $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
        $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

        $item_output = $args->before;
        $item_output .= '<a'. $attributes .'>';
        $item_output .= '<p>' . apply_filters( 'the_title', $item->title, $item->ID ) . '</p>';
        if($item->description) $item_output .= '<p class="sub">' . $item->description . '</p>';
        $item_output .= '</a>';
        $item_output .= $args->after;

        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }
}

function excerpt_read_more_link($output) {
    global $post;
    return $output . '<a class="read-more" href="'. get_permalink($post->ID) . '"> Read More</a>';
}
add_filter('the_excerpt', 'excerpt_read_more_link');

function new_excerpt_more( $more ) {
    return '';
}
add_filter('excerpt_more', 'new_excerpt_more');

function md_nmi_custom_content( $content, $item_id, $original_content ) {
    $content = $content . '<p>' . $original_content . '</p>';
    return $content;
}
add_filter( 'nmi_menu_item_content', 'md_nmi_custom_content', 10, 3 );

function isIE(){

    $isIE = preg_match("/MSIE ([0-9]{1,}[\.0-9]{0,})/",$_SERVER['HTTP_USER_AGENT'],$version);
    if($isIE){
        return $version[1];
    }
    return $isIE;
}

/*
if (isIE()&& isIE() < 9 ){
	//var_dump(isIE());
	header('location:http://www.edge-art.nl');
	die;
}*/

function language_selector_flags(){
    if (function_exists('icl_get_languages')) {
        $languages = icl_get_languages('skip_missing=0&orderby=code&order=desc');
        echo '<section class="lang_selector">';
        if(!empty($languages)){
            foreach($languages as $l){
                $class = $l['active'] ? ' class="active"' : NULL;
                //$langs .= '<section class="flag">';
                $langs .=  '<a ' . $class . ' href="'.$l['url'].'">' . strtoupper ($l['language_code']);
                // $langs .= '<img src="'.$l['country_flag_url'].'" height="12" alt="'.$l['language_code'].'" width="18" /></a>';
                $langs .= '</a>';
                //$langs .= '</section>';

            }
            echo $langs;
        }
        echo '</section>';
    }
}

function my_cpt_post_types( $post_types ) {
    $post_types[] = 'portfolio';
    return $post_types;
}
add_filter( 'cpt_post_types', 'my_cpt_post_types' );
?>
