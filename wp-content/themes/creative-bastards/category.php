<?php get_header();

$current_cat = get_query_var('category_name');
?>

<article class='pagewrap'>
    <section id='pagecontainer' >

    	<?php
    		echo do_shortcode('[ea-post-slider id="slider_about" class="about-us-slider" slide_count=-1 category_name="' . $current_cat . '"]');
		?>

		<script>
			$(function(){
				function onWindowResize(){
					slider_about.onResizeHandler();
					onWindowResizeFunc();
				}
				window.onresize = onWindowResize;
			})
		</script>
	</section>
</article>

<?php get_footer() ?>
