<?php get_header(); 

$current_tax = get_query_var('tax_portfolio');
$args = array(
	'tax_query' => array(
		array(
			'taxonomy' => 'tax_portfolio',
			'field' => 'slug',
			'terms' => $current_tax
		)
	)
);
$query = new WP_Query( $args );
?>

<article class='pagewrap'>
    <section id='pagecontainer' >
		
		<section id='allslider-2' class=''>
			<section id='' class='sliderContainer'>	
				<?php while ( $query->have_posts() ) : $query->the_post(); ?>
					<figure class='sliderElem'>
						<img src='<?= get_featured_image('url', 'full') ?>' />
						<?php the_content(); ?>
						
					</figure>
				<?php endwhile; ?>
			</section>
			<section class='btn-prev'></section>
			<section class='btn-next'></section>
		</section>
		
		<script>
			$(function(){
				$('#pagewrap').css('bottom', '0px')
				$('#pagecontainer').css('max-width', 'initial')
				$('#pagecontainer').css('padding', 'initial')
				var sliderW = $('#pagecontainer').outerWidth(true);
				var sliderH = window.innerHeight-32;
				
				var new_slider = new AllSlider('#allslider-2', sliderW, sliderH, {animType:'slide', animVars:{
					direction:'left',
					speed:5000,
					animSpeed:900
				}});
			})
		</script>
	</section>	
</article> 
	
<?php get_footer() ?>