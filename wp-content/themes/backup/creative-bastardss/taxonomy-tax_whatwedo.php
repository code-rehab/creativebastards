<?php get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>
	<article class='pagewrap' <?= get_featured_image('style', 'full') ?>>
		
		<section id='pagecontainer' >
			<h1 class='page-title color-1'><?php the_title(); ?></h1>
			<?php the_content(); ?>
		</section>
		
	</article> 
	<?php endwhile; ?>
<?php get_footer() ?>