<?php 
function show_countdownfunc($atts) {
    extract(shortcode_atts(array(
            'date' => '2015-01-01'
            ), $atts));
	
	global $countdown_timeleft;
	$countdown_timeleft = $date;
	
	$output = '<section class="edgeart_flipclock"></section>' ;
	add_action('wp_footer', 'add_countdown_script');
    return $output;
}

add_shortcode('show_countdown', 'show_countdownfunc');

function add_countdown_script(){
	global $countdown_timeleft;
	?>
	
	<script>
		var endDate = new Date('<?= $countdown_timeleft ?>')
		var dateNow = new Date();
		var timeleft = Math.abs((endDate - dateNow)/1000)
		
		var clock = $('.edgeart_flipclock').FlipClock(timeleft, {
			clockFace: 'DailyCounter',
			countdown: true
		});
	</script>
	
	<?php
}


?>