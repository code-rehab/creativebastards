<?php 

add_action( 'add_meta_boxes', 'mobile_image_select_metabox_add' );
add_action( 'save_post', 'mobile_image_select_metabox_save' );
function mobile_image_select_metabox_add()
{
    add_meta_box( 'mobile_image_select_metabox', 'Mobile image', 'mobile_image_select_metabox', 'post', 'normal', 'high' );
}

function mobile_image_select_metabox( $post )
{
	$values = get_post_custom( $post->ID );
	$mobile_image_url = isset( $values['mobile_image_url'] ) ? esc_attr( $values['mobile_image_url'][0] ) : "";
	
?>
	
	<div class='mobile-img-container'>
		<?php 
			if($mobile_image_url){
				echo '<img src="' . $mobile_image_url . '" width="100%" />';
			}
		?>
	</div>

	<p>
		<a href='#' id="select_image">Mobiele afbeelding selecteren</a>
		<input type="hidden" name='mobile_image_url' id='mobile_image_url' value="<?= $mobile_image_url ?>" />
	</p>
	
    
    <script>
	    jQuery('#select_image').click(function(e){
	    	wp.media.editor.open(jQuery('#labels_props'));
	    	wp.media.editor.send.attachment = function(props, attachment){
	    		jQuery('.mobile-img-container').html('<img src="' + attachment.url + '" width="100%" />');
	    		jQuery('#mobile_image_url').val(attachment.url)
	    		console.log(props, attachment);
	    	}
	    });
    </script>
<?php   

	
}

function mobile_image_select_metabox_save( $post_id )
{
    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post' ) ) return;
	
	
	if( isset( $_POST['mobile_image_url'] ) )
		update_post_meta( $post_id, 'mobile_image_url', $_POST['mobile_image_url']);
}

?>