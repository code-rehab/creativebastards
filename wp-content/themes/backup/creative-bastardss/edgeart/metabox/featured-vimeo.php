<?php

add_action( 'add_meta_boxes', 'vimeo_metabox_add' );
add_action( 'save_post', 'vimeo_metabox_save' );
function vimeo_metabox_add()
{
    add_meta_box( 'vimeo_metabox', 'Vimeo featured', 'vimeo_metabox', 'portfolio', 'normal', 'high' );
}

function vimeo_metabox( $post )
{
	$values = get_post_custom( $post->ID );
	$vimeo_url = isset( $values['vimeo_url'] ) ? esc_attr( $values['vimeo_url'][0] ) : "";
?>

	<p>
		<label for="">Url</label><br />
		<input type="text" name='vimeo_url' id='vimeo_url' value="<?= $vimeo_url ?>" style="width:100%"/>
	</p>

    <script>
	    jQuery('#select_image').click(function(e){
	    	wp.media.editor.open(jQuery('#labels_props'));
	    	wp.media.editor.send.attachment = function(props, attachment){
	    		jQuery('.mobile-img-container').html('<img src="' + attachment.url + '" width="100%" />');
	    		jQuery('#mobile_image_url').val(attachment.url)
	    		console.log(props, attachment);
	    	}
	    });
    </script>
<?php
}

function vimeo_metabox_save( $post_id )
{
    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post' ) ) return;


	if( isset( $_POST['vimeo_url'] ) )
		update_post_meta( $post_id, 'vimeo_url', $_POST['vimeo_url']);

	
}

?>