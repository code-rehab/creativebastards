<?php 

add_action( 'add_meta_boxes', 'slide_properties_meta_box_add' );
add_action( 'save_post', 'slide_properties_meta_box_save' );
function slide_properties_meta_box_add()
{
    add_meta_box( 'slide_properties-menu-select', 'Slide options', 'slide_properties_examples_menu_box', 'post', 'normal', 'high' );
	add_meta_box( 'slide_properties-menu-select', 'Slide options', 'slide_properties_examples_menu_box', 'portfolio', 'normal', 'high' );
}

function slide_properties_examples_menu_box( $post )
{
	$values = get_post_custom( $post->ID );
	
	$has_labels = isset( $values['has_labels'] ) ? esc_attr( $values['has_labels'][0] ) : "";
	$has_description = isset( $values['has_description'] ) ? esc_attr( $values['has_description'][0] ) : "";
	
	$heading1 = isset( $values['heading1'] ) ? esc_attr( $values['heading1'][0] ) : "";
	$heading2 = isset( $values['heading2'] ) ? esc_attr( $values['heading2'][0] ) : "";
	
?>
	<p>
		<input type="checkbox" name="has_description" id="has_description" <?php if ($has_description) echo 'checked'?>/><label for="has_description">show description</label>
	</p>
	<p>
		<input type="checkbox" name="has_labels" id="has_labels" <?php if ($has_labels) echo 'checked'?>/><label for="has_labels">show labels</label>
	</p>
	
	<div id='labels_props'>
		<p>
		    <label for="">Heading 1</label><br />
		    <input type="text" name="heading1" id="heading1" value="<?php echo $heading1; ?>" />
	    </p>
	    <p>
		    <label for="">Heading 2</label><br />
		    <input type="text" name="heading2" id="heading2" value="<?php echo $heading2; ?>" />
	    </p>
    </div>
    
    <script>
    
	    if (!jQuery('#has_labels').prop('checked')) jQuery('#labels_props').hide();
	    jQuery('#has_labels').change(function(e){
	    	jQuery('#labels_props').toggle();
	    });
	    
    </script>
<?php   

	
}

function slide_properties_meta_box_save( $post_id )
{
    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post' ) ) return;
	
	if( isset( $_POST['has_labels'] ) ){
		$has_labels = $_POST['has_labels'] ? true : false;
		update_post_meta( $post_id, 'has_labels', $has_labels );
	}
	if( isset( $_POST['has_description'] ) ){
		$has_description = $_POST['has_description'] ? true : false;
		update_post_meta( $post_id, 'has_description', $has_description );
	}
	if( isset( $_POST['heading1'] ) )
		update_post_meta( $post_id, 'heading1', $_POST['heading1']);
		
	if( isset( $_POST['heading2'] ) )
		update_post_meta( $post_id, 'heading2', $_POST['heading2']);
}

?>