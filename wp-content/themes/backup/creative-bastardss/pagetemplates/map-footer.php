<?php
/*
Template Name: map-footer
*/
get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>
	<article class='pagewrap' <?= get_featured_image('style', 'full') ?>>
		
		<section id='pagecontainer' >
			<h1 class='page-title color-1'><?php the_title(); ?></h1>
			<?php the_content(); ?>
		</section>
		
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2444.261753114988!2d6.874075799999994!3d52.22046209999993!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47b8138dfa4a06c7%3A0x4a547ca1850075a8!2sParkweg+55%2C+7545+MT+Enschede!5e0!3m2!1snl!2snl!4v1413386374876" width="100%" height="50%" frameborder="0" style="border:0"></iframe>
	</article> 
	<?php endwhile; ?>
<?php get_footer() ?>