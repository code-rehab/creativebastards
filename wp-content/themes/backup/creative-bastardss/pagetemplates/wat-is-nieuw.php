<?php
/*
Template Name: What's new
*/
get_header();
?>

<article class='pagewrap'>
    <section id='pagecontainer'  style="overflow:hidden">

		<?php 
			echo do_shortcode('[ea-post-slider id="slider_news" class="watsnew_slider" category_name="whats-new" title="What’s new"]');
			echo do_shortcode('[ea-post-slider id="slider_about" class="watsnew_slider" category_name="about-us" title="About us" no_slide_description=true]');
			echo do_shortcode('[ea-post-slider id="slider_services" class="watsnew_slider" post_type="portfolio" title="What we do" orderby="rand" no_slide_description=true]');
		?>
		
		<script>
			$(function(){
				function onWindowResize(){
					slider_news.onResizeHandler();
					slider_about.onResizeHandler();
					slider_services.onResizeHandler();
					onWindowResizeFunc();
				}
				window.onresize = onWindowResize;
			})
		</script>
		
	</section>	
</article> 

<?php get_footer() ?>



