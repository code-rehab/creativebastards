<?php get_header(); 
	$current_tax = get_query_var('tax_portfolio');
	$args = array(
		'tax_query' => array(
			array(
				'taxonomy' => 'tax_portfolio',
				'field' => 'slug',
				'terms' => $current_tax
			)
		)
	);
	$query = new WP_Query( $args );
?>

<article class='pagewrap'>
    <section id='pagecontainer' >
		
		<section id='allslider' class='portfolio-slider'>
			<section id='' class='sliderContainer'>	
				<?php while ( $query->have_posts() ) : $query->the_post(); 
					
					$cta_title = get_post_meta( get_the_ID(), 'cta_title', true );
					$cta_content = get_post_meta( get_the_ID(), 'cta_content', true );
					
				?>
					<figure class='sliderElem'>
						<section class='imageholder'>
							<img class='slideimage' src='<?= get_featured_image('url', 'full') ?>' />
						</section>
						<a class='overlaybutton' href='<?= get_permalink(get_the_ID()) ?>'></a>
						
						<section class='description desktop'>
							<section class='inner '>
								<section class='item-description'>
									<h2 class='title'><?php the_title(); ?></h2>
									<p><?php the_excerpt(); ?></p>
									<span class='social-icons'>
							
											<li class='icon pinterest'>

											<?php 

												$url = get_permalink(get_the_ID());

												echo '<a onclick="';
												echo "window.open('http://pinterest.com/pin/create/button/?url=";
												echo $url;
												echo '&amp;media=' . get_featured_image('url', 'full');
												echo '&amp;description=Merk: ' . $url;
												echo "', 'pinit', 'height=320, width=640, toolbar=no, menubar=no, scrollbars=no, resizable=no, location=no, directories=no, status=no');";
												echo '" href="javascript: void(0);">';
												echo '</a>';
											?>

											</li>

											<li class='icon facebook'>
											<?php 

												
												echo '<a style="margin-right:10px;" class="icon facebook" onclick="';
												echo "window.open('https://www.facebook.com/sharer/sharer.php?u=" . $url . "', 'facebook_share', 'height=320, width=640, toolbar=no, menubar=no, scrollbars=no, resizable=no, location=no, directories=no, status=no');";
												echo '" href="javascript: void(0);">';
												echo '</a>';
											?>


											</li>
											<li class='icon twitter'>
												<script>
												$(document).ready(function(){
													function tweet_btn(){
														// We bind a new event to our link
														$('a.tweet').click(function(e){
														 
														  //We tell our browser not to follow that link
														  e.preventDefault();
														 
														  //We get the URL of the link
														  var loc = $(this).attr('href');
														 
														  //We get the title of the link
														  var title  = escape($(this).attr('title'));
														  
														  //We trigger a new window with the Twitter dialog, in the middle of the page
														  window.open('http://twitter.com/share?url=' + loc + '&text=' + title + '&', 'twitterwindow', 'height=450, width=550, top='+($(window).height()/2 - 225) +', left='+$(window).width()/2 +', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
														});
													}

													tweet_btn();
												});
												</script>
												<?php 
													echo '<a style="margin-right:10px;" href="' . $url . '" title="" class="tweet" target="_blank"></a>';
													
												?>

											</li>
										</ul>
									</span>
								</section>
								<section class='call-to-action'>
									<h2><?= $cta_title ?></h2>
									<p><?= $cta_content ?></p>
									<blockquote>
										<h3><a href="mailto:info@creativebastards.nl">Connect</a></h3>
									</blockquote>
								</section>
							</section>
							<section class='btn-prev'></section>
							<section class='btn-next'></section>
						</section>
						
						<section class='description mobile'>
							<section class='inner '>
								<h2 class='title'><?php the_title(); ?></h2>
							</section>
								<a class='read-more' href='<?= get_permalink(get_the_ID()) ?>'>View details</a>
								<?php if ($device->isMobile() && !$device->isTablet()){?>
									<a class='contact-us phone' href='tel:+31742502299'></a>
								<?php }else{ ?>
									<a class='contact-us mail' href='mailto:<?= the_author_email() ?>'></a>
								<?php } ?>
							
						</section>
						 
					</figure>
				<?php endwhile; ?>
			</section>
			<section class="bullets"></section>
		</section>
		
		<script>
			$(function(){
				var slider = new AllSlider('#allslider', -1, -1, {animType:'slide', animVars:{
					direction:'left',
					descriptionAnim:'none',
					speed:7000,
					animSpeed:900
				}});
				
				function onWindowResize(){
					slider.onResizeHandler();;
					onWindowResizeFunc();
				}
				window.onresize = onWindowResize;
			})
		</script>
	</section>	
</article> 
	
<?php get_footer() ?>