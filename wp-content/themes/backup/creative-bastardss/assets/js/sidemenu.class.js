var EA_CBsidemenu = function(wp_menu_elem, vars){
	
	this.elementID = wp_menu_elem;
	this.element = $(this.elementID);
	this.isOpen = vars.isOpen ? true : false;
	this.currentlevel = 0;
	this.menus = Array(this);
	
	this.init = function(){
		var swipeHandler = this.swipeHandler.bind(this);
		this.element.find('.sliderContainer').swipe({excludedElements:"", swipeStatus:swipeHandler});
		/*
		var submenus = this.element.find('.sub-menu');
		$.each(submenus, function(i, submenu){
			this.menus.push($(submenu));
		}.bind(this));
		
		console.log(this.menus);*/
		var mItem_link = $('.menu', this.element).children('.menu-item').find('a:first');
		mItem_link.click(this.open.bind(this));
		
		sub_mItem_link = $('aside#main-menu .sub-menu').children('.menu-item').find('a:first');
		sub_mItem_link.click(this.open.bind(this));
		
		this.onresize();
	};
	
	this.open = function(e){
		
		if (this.currentlevel == 0){
			this.element.addClass('opened');
			this.currentlevel ++;
			this.isOpen = true;
		}else{
		
			var mItem = $(e.target).closest('.menu-item');
			var submenu = mItem.find('.sub-menu:first');
			
			if(mItem.hasClass('menu-item-has-children')) {
				if (this.currentlevel == 2){
					$('aside#main-menu .sub-menu').removeClass('expanded');
				} else{
					$('aside#main-menu .sub-menu .sub-menu').removeClass('expanded');
					submenu.css('height', (submenu.children('.menu-item').length*28)+'px');
					var newMenuHeight = ((submenu.children('.menu-item').length*28)+$('aside#main-menu nav').outerHeight())-200;
					$('aside#main-menu nav').css('margin-top', '-' + newMenuHeight/2 + 'px');
				}
				submenu.addClass('expanded');
				
				e.preventDefault();	
			}
		}
	};
	
	this.close = function(sublevel){
		if (this.isOpen || this.currentlevel > 0){
			$('aside#main-menu .sub-menu').removeClass('expanded');
			this.element.removeClass('opened');
			this.currentlevel = 0;
			this.isOpen = false;
		}else{
			this.open();
		}
	};
	
	this.toggle = function(){
		if (this.isOpen) this.close();
		else this.open();
	};
	
	this.swipeHandler = function(event, phase, direction, distance){
		
		//enable clicking
		if ($(event.target).attr('href') && distance < 7) {
			if (phase == "end" || phase == "cancel"){
				$(event.target).unbind('click').click();
				return;
			}
		}
		
		//close menu or submenu
		if (phase == "end" || phase == "cancel"){
			if (direction == 'left')
				this.close(this.currentlevel);
		}
	};
	
	this.onresize = function(){
		$.each($('aside#main-menu .sub-menu .sub-menu'), function(i, submenu){
			var menuItem = $(submenu).find('.menu-item');
			var itemInner = $(submenu).find('.menu-item a');
			var itemHeight = $(submenu).innerHeight()/menuItem.length;
			
			menuItem.css('height', itemHeight+'px');
			itemInner.css('padding-top', itemHeight-120+'px');
		});	
	};
	
	this.init();
};
