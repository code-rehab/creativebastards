<?php get_header(); 

$current_cat = get_query_var('cat');
$args = array(
	'cat' => $current_cat
);
$query = new WP_Query( $args );
?>

<article class='pagewrap'>
    <section id='pagecontainer' >
		
		<section id='allslider-2' class='about-us-slider'>
			<section id='' class='sliderContainer'>	
				<?php while ( $query->have_posts() ) : $query->the_post(); ?>
					<figure class='sliderElem'>
						<section class="imageholder">
							<img class='slideimage' src='<?= get_featured_image('url', 'full') ?>' />
						</section>
						<section class='description'>
						<section class='inner'>
							<h2><?php the_title(); ?></h2>
							<?php the_excerpt(); ?>
						</section>
						<section class='btn-prev'></section>
						<section class='btn-next'></section>
					</section>
						
					</figure>
				<?php endwhile; ?>
			</section>
		</section>
		
		<script>
			$(function(){
				$('#pagewrap').css('bottom', '0px')
				$('#pagecontainer').css('max-width', 'initial')
				$('#pagecontainer').css('padding', 'initial')
				var sliderW = $('#pagecontainer').outerWidth(true);
				var sliderH = window.innerHeight-2
				
				var slider = new AllSlider('#allslider-2', sliderW, sliderH, {animType:'slide', animVars:{
					direction:'left',
					descriptionAnim:'slideUp',
					speed:5000,
					animSpeed:900
				}});
				
				window.onresize = function(){
					var sliderW = $('#pagecontainer').outerWidth(true);
					var sliderH = window.innerHeight-2;
					slider.element.css('width', sliderW);
					slider.element.css('height', sliderH);
					slider.onResizeHandler();
				}
			})
		</script>
	</section>	
</article> 
	
<?php get_footer() ?>